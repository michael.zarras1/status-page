const path = require('path');

module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        '~': path.resolve(__dirname, 'src'),
      },
    },
  },
  css: {
    loaderOptions: {
      scss: {
        sassOptions: {
          includePaths: ['node_modules', '.'],
        },
      },
    },
  },
  devServer: {
    allowedHosts: 'all',
  },
};
