import { shallowMount } from '@vue/test-utils';
import Vue from 'vue';
import VueSanitize from 'vue-sanitize';
import { GlMarkdown } from '~/components';

describe('Markdown text', () => {
  let wrapper;
  Vue.use(VueSanitize);

  function mountComponent() {
    wrapper = shallowMount(GlMarkdown, {
      propsData: {
        text: "<h3>This is sanitized - no XSS</h3>\n<body onload=alert(‘XSS on load’)>\n<script>alert('XSS')</script>",
      },
    });
  }
  beforeEach(() => {
    mountComponent();
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  it('should render whitelisted tags', () => {
    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.html()).toContain('h3');
  });

  it('should sanitize unsafe tags', () => {
    expect(wrapper.html()).not.toContain('script');
    expect(wrapper.html()).not.toContain('body');
  });
});
