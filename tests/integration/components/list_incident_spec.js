import { GlTooltip, GlBadge } from '@gitlab/ui';
import { shallowMount, RouterLinkStub } from '@vue/test-utils';
import { GlListIncident } from '~/components';
import { dateTimeMixin } from '~/utils/datetime';

jest.mock('~/utils/datetime');

const formattedDate = 'Feb 21, 2020';
const formattedDateTime = 'Feb 21, 2020 9:20am';
const formattedTimezone = 'GMT+2';

dateTimeMixin.methods.formatDate.mockReturnValue(formattedDate);
dateTimeMixin.methods.formatDateTime.mockReturnValue(formattedDateTime);
dateTimeMixin.methods.formatDateTimeZone.mockReturnValue(formattedTimezone);

describe('List Incident', () => {
  let wrapper;

  function mountComponent() {
    wrapper = shallowMount(GlListIncident, {
      propsData: {
        incident: {
          id: 3,
          status: 'closed',
          title: 'Wrong issue order',
          description: '',
          updated_at: '2020-02-21T07:10:46.097Z',
          created_at: '2020-02-21T07:05:38.261Z',
          links: { details: 'data/incident/3.json' },
        },
      },
      stubs: { RouterLink: RouterLinkStub },
    });
  }

  beforeEach(() => {
    mountComponent();
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  const findBadge = () => wrapper.findComponent(GlBadge);
  const findDate = () => wrapper.findComponent({ ref: 'createdAtDateTime' });
  const findTooltip = () => wrapper.findComponent(GlTooltip);

  it('renders the component', () => {
    expect(wrapper.element).toMatchSnapshot();
  });

  it('renders status badge', () => {
    expect(findBadge().exists()).toBe(true);
    expect(findBadge().classes()).toContain('text-capitalize');
  });

  it('renders posted at date with tooltip', () => {
    expect(findDate().exists()).toBe(true);
    expect(findDate().text()).toContain(formattedDate);
    expect(findTooltip().exists()).toBe(true);
    expect(findTooltip().text()).toContain(`${formattedDateTime} ${formattedTimezone}`);
  });
});
