#!/bin/sh

# Create S3 a bucket (if not preexisting) and configure it as a website

set -e

DIR="$( cd "$( dirname "$0" )" && pwd )"
# shellcheck disable=1090
. "$DIR/vars"

# S3 bucket
has_bucket=$(aws s3 ls | grep -E "\\s+$S3_BUCKET_NAME\$" || true)

if [ "$has_bucket" ]
then
  echo "$has_bucket"
else
  aws s3 mb "s3://$S3_BUCKET_NAME"
fi

# Bucket policy
has_policy=$(aws s3api get-bucket-policy --bucket "$S3_BUCKET_NAME" 2>/dev/null || true)

if [ "$has_policy" ]; then
  echo "Bucket policy already defined"
else
  # Create bucket policy
  policy=$(sed "s/\$S3_BUCKET_NAME/$S3_BUCKET_NAME/g" "$DIR/etc/bucket_policy.json")
  echo "Setting bucket policy"
  aws s3api put-bucket-policy --bucket "$S3_BUCKET_NAME" --policy "$policy"
fi

lifecycle=$(sed "s/\$S3_BUCKET_NAME/$S3_BUCKET_NAME/g" "$DIR/etc/bucket_lifecycle.json")
aws s3api put-bucket-lifecycle-configuration \
  --bucket "$S3_BUCKET_NAME" \
  --lifecycle-configuration "$lifecycle"

# Create website
aws s3 website "s3://$S3_BUCKET_NAME/" --index-document index.html --error-document index.html

echo "Website at $AWS_WEBSITE"
